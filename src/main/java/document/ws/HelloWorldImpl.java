package document.ws;

import javax.jws.WebService;

/**
 * Service Implement
 * */
@WebService(endpointInterface = "com.minhnd.ws.document.HelloWorld")
public class HelloWorldImpl implements HelloWorld {
	
	@Override
	public String getHelloWorldAsString(String name) {
		return "Hello webservice: " + name;
	}
	
}
