package rpc.ws.endpoint;

import javax.xml.ws.Endpoint;

import rpc.ws.HelloWorldImpl;

/**Endpoint publisher*/
public class HelloWorldPublisher {
	
	public static void main(String[] args) {
		
		Endpoint.publish("http://localhost:9999/ws/rpc/hello", new HelloWorldImpl());
		System.out.println(".......Start Service.......RPC");
	}
	
}